import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

public class SETEST {
	 
  @DataProvider(name = "Update-Data")
  public static  Object[][] updateData() {
	  
	  Account acc1=new Account("Sayed","123","mohamedsh@gm.com","m",30,"teacher"); 
	  Account acc2= new Account("mohamed","","mohamedsh@gm.com","m",21,"student");   
	  Account acc3= new Account("Maryam","123","moham@gm.com","F",20,"teacher");  
	  Account acc4= new Account("","678","moham","m",20,"teacher");  
	  Account acc5= new Account("mohamed","123","moham@gm.com","",0,"student") ;  
	  return new Object [][]{ {1, acc1 }, {null, acc2 }, {1, acc3}, {null, acc4}, {null,acc5} };

  }
  
  
  Registeration reg = new Registeration();
  
  
  @DataProvider(name = "Upload-Game")
public static  Object[][] uploadGame() {
	  
	  Game game1=new Game("Math&Scince","TF",12,20,"game1.txt"); 
	  Game game2= new Game("Math","MCQ",12,20,"game2.txt");   
	  Game game3= new Game("","",12,20,"game3.txt");  
	  Game game4= new Game("Math&Scince","TF",12,20,"");  
	  Game game5= new Game("","",0,0,"") ;  
	  return new Object [][]{ {true, game1 }, {true, game2 }, {true, game3}, {false,game4}, {false,game5} };

  }
  
  
  Admin admin= new Admin();
  

  @DataProvider(name = "add-Game")
public static  Object[][] addGame() {
	  
	  Game game1=new Game("Math&Scince","TF",12,20,"game1.txt"); 
	  Game game2= new Game("Math","MCQ",12,20,"game2.txt");   
	  Game game3= new Game("","",12,20,"game3.txt");  
	  Game game4= new Game("Math&Scince","TF",12,20,"");  
	  Game game5= new Game("","",0,0,"") ;
	  return new Object [][]{ {true, game1 }, {true, game2 }, {false, game3}, {false,game4}, {false,game5} };
  }
  
  
  
  @DataProvider(name = "sign-in")
public static  Object[][] signIn() {
	  
	  return new Object [][]{ {0, "ay7aga@gmail.com","123"}, {1, "mk@gmail.com","123" } };
  }
  
//  Registeration Register= new Registeration();
//  
//  @Test(dataProvider="sign-in")
//  public void testSignIn(int test,String mail,String pass)
//  {
//	  Assert.assertEquals(test, Register.Signin(mail, pass));
//  }
  
  
  AddGame g = new AddGame();
  
  @Test(dataProvider="add-Game")
  public void TestAddGame(boolean test, Game game)
  {
	  admin.ID = "20140";
	  admin.Name = "Amr";
	  Assert.assertEquals(test,g.AddGame(game,admin));
  }	
  
  Data_manager DM=new Data_manager();
  
  
//  @Test(dataProvider="Upload-Game")
//  public void TestUploadGame(boolean test, Game game)
//  {
//	  Assert.assertEquals(test,DM.upload_game(game) );
//  }	
  
  
  
//  @Test(dataProvider="Update-Data")
//  public void TestUpdateData(boolean test, Account acc)
//  {
//	  Assert.assertEquals(test,DM.update_data(acc) );
//  }	
//
  
//@Test(dataProvider="Upload-Game")
//public void signIn(int res, Account acc)
//{
//	  Assert.assertEquals(res,reg.Signin());
//}
  
  


}
